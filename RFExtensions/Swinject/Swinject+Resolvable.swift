//
//  Swinject+Resolvable.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 09.01.2021.
//  Copyright © 2021 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

public protocol Resolvable: Injectable { }

public protocol Resolvable1: Injectable { }

public protocol Resolvable2: Injectable { }

public protocol Resolvable3: Injectable { }

public protocol Resolvable4: Injectable { }

public protocol Resolvable5: Injectable { }

public protocol Resolvable6: Injectable { }

public protocol Resolvable7: Injectable { }

public protocol Resolvable8: Injectable { }

public protocol Resolvable9: Injectable { }

public protocol Resolvable10: Injectable { }
