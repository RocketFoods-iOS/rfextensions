//
//  Swinject+ResolvableOptional.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 09.01.2021.
//  Copyright © 2021 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

public protocol ResolvableOptional: Injectable { }

public protocol ResolvableOptional1: Injectable { }

public protocol ResolvableOptional2: Injectable { }

public protocol ResolvableOptional3: Injectable { }

public protocol ResolvableOptional4: Injectable { }

public protocol ResolvableOptional5: Injectable { }

public protocol ResolvableOptional6: Injectable { }

public protocol ResolvableOptional7: Injectable { }

public protocol ResolvableOptional8: Injectable { }

public protocol ResolvableOptional9: Injectable { }

public protocol ResolvableOptional10: Injectable { }
