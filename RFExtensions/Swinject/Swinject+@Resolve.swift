//
//  Swinject+@Resolve.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 01.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

@propertyWrapper
public final class Resolve<Service: Injectable>: InjectableProperty {
    private var service: (() -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: (() -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(Service.self)
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve<Service> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve1<Service: Injectable, Arg1>: InjectableProperty {
    private var service: ((Arg1) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(Service.self, argument: $0)
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve1<Service, Arg1> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve2<Service: Injectable, Arg1, Arg2>: InjectableProperty {
    private var service: ((Arg1, Arg2) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve2<Service, Arg1, Arg2> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve3<Service: Injectable, Arg1, Arg2, Arg3>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve3<Service, Arg1, Arg2, Arg3> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve4<Service: Injectable,
                                    Arg1, Arg2, Arg3, Arg4>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3, Arg4) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve4<Service, Arg1,
                                                Arg2, Arg3, Arg4> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve5<Service: Injectable, Arg1,
                                    Arg2, Arg3, Arg4, Arg5>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4, Arg5) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3, Arg4, Arg5) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3, $4
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve5<Service, Arg1,
                                                Arg2, Arg3, Arg4, Arg5> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve6<Service: Injectable, Arg1,
                                    Arg2, Arg3, Arg4, Arg5, Arg6>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3, $4, $5
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve6<Service, Arg1, Arg2,
                                                Arg3, Arg4, Arg5, Arg6> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve7<Service: Injectable, Arg1,
                                    Arg2, Arg3, Arg4, Arg5, Arg6, Arg7>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3, $4, $5, $6
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve7<Service, Arg1, Arg2,
                                                Arg3, Arg4, Arg5, Arg6, Arg7> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve8<Service: Injectable, Arg1, Arg2,
                                    Arg3, Arg4, Arg5, Arg6, Arg7, Arg8>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3,
                                Arg4, Arg5, Arg6, Arg7, Arg8) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3, $4, $5, $6, $7
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve8<Service, Arg1, Arg2,
                                                Arg3, Arg4, Arg5, Arg6, Arg7, Arg8> {
        get { self }
    }
}

@propertyWrapper
public final class Resolve9<Service: Injectable, Arg1, Arg2,
                                    Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9>: InjectableProperty {
    private var service: ((Arg1, Arg2, Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) -> Service?)!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: ((Arg1, Arg2, Arg3,
                                Arg4, Arg5, Arg6, Arg7, Arg8, Arg9) -> Service?) {
        get {
            guard service != nil else {
                service = { [weak self] in
                    self?.modules?.synchronize().resolve(
                        Service.self,
                        arguments: $0, $1, $2, $3, $4, $5, $6, $7, $8
                    )
                }
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Resolve9<Service, Arg1, Arg2,
                                                Arg3, Arg4, Arg5, Arg6, Arg7, Arg8, Arg9> {
        get { self }
    }
}
