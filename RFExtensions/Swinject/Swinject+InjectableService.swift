//
//  Swinject+InjectableService.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 01.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

public protocol InjectableService: Injectable {
    init()
}

public extension InjectableService {
    static func register(in container: Container) {
        container.register(Self.self) { _ in
            Self.init()
        }
    }
}

public extension Array where Element == InjectableService.Type {
    func registerAll(in modules: Container) {
        forEach { $0.register(in: modules) }
    }
}
