//
//  Swinject+Injectable.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 31.08.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

public protocol Injectable: class { }

public protocol InjectableProperty {
    var modules: Container? { get set }
}
