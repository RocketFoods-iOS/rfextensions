//
//  Swinject+Inject.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 01.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

@propertyWrapper
public final class Inject<Service: Injectable>: InjectableProperty {
    private var service: Service!
    public var modules: Container?
    
    public init() { }
    
    public init(modules: Container?) {
        self.modules = modules
    }
    
    public var wrappedValue: Service {
        get {
            guard service != nil else {
                service = modules?.synchronize().resolve(Service.self)
                
                return service
            }
            
            return service
        }
        
        set { service = newValue }
    }
    
    public var projectedValue: Inject<Service> {
        get { self }
    }
}
