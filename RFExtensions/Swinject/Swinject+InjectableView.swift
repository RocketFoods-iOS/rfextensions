//
//  Swinject+InjectableView.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 01.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import Swinject

public protocol InjectableView: Injectable { }
