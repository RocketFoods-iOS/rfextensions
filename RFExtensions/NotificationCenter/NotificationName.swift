//
//  NotificationName.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 01.08.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension Notification.Name {
    static let didChangeAuth = Notification.Name("didChangeAuth")
    
    static let didGrantLocationPermision = Notification.Name("didGrantLocationPermision")
}
