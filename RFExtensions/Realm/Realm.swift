//
//  Realm.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 09.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation
import RealmSwift

public extension Realm {
    func add<P: Persistable>(_ value: P) {
        add(value.managedObject)
    }

    func add<S>(_ values: S, page: Int? = 0) where S: Sequence, S.Element: Persistable {
        let objects = List<S.Element.ManagedObject>()
            
        values.forEach { value in
            objects.append(value.managedObject)
        }
        
        func sortByPersistOrder<O: Object>(objects: List<O>) {
            for (i, object) in objects.enumerated() {
                guard object is ManagedObject else { return }
                
                object.setValue(i, forKey: "persistOrderId")
                object.setValue(object.value(forKey: "persistPage") ?? page, forKey: "persistPage")
                
                func sortAllProperties<PO: Object>(in object: PO) {
                    let objectType = type(of: object)

                    let primaryKey = objectType.primaryKey()
                    let primaryKeyValue = primaryKey.flatMap(object.value(forKey:))
                    
                    for property in object.objectSchema.properties {
                        switch object.value(forKey: property.name) {
                        case let realmObject as Object:
                            sortAllProperties(in: realmObject)
                        default: //LinkingObjects
                            guard !property.isArray else {
                                for realmObject in object.dynamicList(property.name) {
                                    sortAllProperties(in: realmObject)
                                }
                                
                                return
                            }
                            
                            let linkOriginPropertyName = property.name
                            
                            if let linkOriginTypeName = property.objectClassName,
                               let primaryKey = primaryKey,
                               let primaryKeyValue = primaryKeyValue {
                                dynamicObjects(linkOriginTypeName)
//                                    .filter("%K == %@", "\(linkOriginPropertyName).\(primaryKey)", primaryKeyValue)
                                    .filter("%K == %@", "\(primaryKey)", primaryKeyValue) // TODO: Refactor
                                    .forEach { sortAllProperties(in: $0) }
                            }
                        }
                    }
                }
                
                sortAllProperties(in: object)
            }
        }
        
        sortByPersistOrder(objects: objects)
        
        add(objects)
    }
    
    private func cascadeDelete(_ object: Object & ManagedObject) {
//        let objectType = type(of: object)

//        let primaryKey = objectType.primaryKey()
//        let primaryKeyValue = primaryKey.flatMap(object.value(forKey:))
        
        for property in object.objectSchema.properties {
            switch object.value(forKey: property.name) {
            case let realmObject as Object & ManagedObject:
                cascadeDelete(realmObject)
            default: //LinkingObjects
                guard !property.isArray else {
                    for realmObject in object.dynamicList(property.name) {
                        guard let realmObject = realmObject as? Object & ManagedObject else {
                            continue
                        }
                        
                        cascadeDelete(realmObject)
                    }
                    
                    return
                }
                
                continue
//                let linkOriginPropertyName = property.name
//
//                if let linkOriginTypeName = property.objectClassName,
//                   let primaryKey = primaryKey,
//                   let primaryKeyValue = primaryKeyValue {
//                    dynamicObjects(linkOriginTypeName)
//                        .filter("%K == %@", "\(linkOriginPropertyName).\(primaryKey)", primaryKeyValue) // Crash: Terminating app due to uncaught exception 'Invalid property name', reason: 'Property 'baseLotDto' not found in object of type 'BaseLotDtoObject''
//                        .forEach { dynamicObject in
//                            guard let dynamicObject = dynamicObject as? Object & ManagedObject else { return }
//
//                            cascadeDelete(dynamicObject) }
//                }
            }
        }
        
//        for property in object.objectSchema.properties {
//            if property.isArray {
//                delete(object.dynamicList(property.name), cascading: true)
//            } else if let propertyObject = object.value(forKey: property.name) as? Object & ManagedObject {
//                delete(propertyObject, cascading: true)
//            }
//        }
        
        delete(object)
    }
    
    func delete(_ object: Object & ManagedObject, cascading: Bool) {
        if cascading {
            cascadeDelete(object)
        } else {
            delete(object)
        }
    }
    
    func delete<S>(_ objects: S, cascading: Bool) where S : Sequence, S.Element : Object & ManagedObject {
        if cascading {
            objects.forEach { delete($0, cascading: true) }
        } else {
            delete(objects)
        }
    }

    func delete<Element>(_ objects: List<Element>, cascading: Bool) where Element : Object & ManagedObject, Element : RealmCollectionValue {
        if cascading {
            objects.forEach { delete($0, cascading: true) }
        } else {
            delete(objects)
        }
    }

    func delete<Element>(_ objects: Results<Element>, cascading: Bool) where Element : Object & ManagedObject, Element : RealmCollectionValue {
        if cascading {
            objects.forEach { delete($0, cascading: true) }
        } else {
            delete(objects)
        }
    }

    func delete<P: Persistable>(_ value: P, cascading: Bool = false) where P.ManagedObject : ManagedObject {
        delete(value.managedObject, cascading: cascading)
    }

    func delete<S>(_ values: S, cascading: Bool = false) where S: Sequence, S.Element: Persistable, S.Element.ManagedObject : ManagedObject {
        delete(values.compactMap { $0.managedObject }, cascading: cascading)
    }
    
    func objects<P: Persistable>(_ type: P.Type) -> Results<P.ManagedObject> {
        objects(P.ManagedObject.self)
    }
}

public extension Results where Element: KeypathSortable {
    var sortedByPersistOrder: Results<Element> {
        sorted(byKeyPath: "persistOrderId")
    }
}

public extension LazyFilterSequence where Element: RealmCollectionValue & ManagedObject {
    var sortedByPersistOrder: [Element] {
        sorted {
            guard let leftValue = $0.persistOrderId.value,
                let rightValue = $1.persistOrderId.value else { return false }
            
            return leftValue < rightValue
        }
    }
}

public extension Sequence where Element: RealmCollectionValue & ManagedObject {
    var sortedByPersistOrder: [Element] {
        sorted {
            guard let leftValue = $0.persistOrderId.value,
                let rightValue = $1.persistOrderId.value else { return false }
            
            return leftValue < rightValue
        }
    }
}
