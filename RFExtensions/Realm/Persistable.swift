//
//  Persistable.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 09.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation
import RealmSwift

public protocol Persistable {
    associatedtype ManagedObject: RealmSwift.Object

    init?(_ managedObject: ManagedObject?)
    
    init(_ managedObject: ManagedObject)

    var managedObject: ManagedObject { get }
    
    var persistOrderId: Int? { get set }
    var persistPage: Int? { get set }
}

public extension Array where Element: Persistable {
    mutating func setPersistOrder(page: Int = 0) {
        for (i, _) in self.enumerated() {
            self[i].persistOrderId = i
            self[i].persistPage = page
        }
    }
}

public protocol ManagedObject {
    var persistOrderId: RealmProperty<Int?> { get }
    var persistPage: RealmProperty<Int?> { get }
}

public extension LazyFilterSequence where Element: Persistable {
    var sortedByPersistOrder: [Element] {
        sorted {
            guard let leftValue = $0.persistOrderId,
                  let rightValue = $1.persistOrderId else { return false }
            
            return leftValue < rightValue
        }
    }
}

public extension Sequence where Element: Persistable {
    var sortedByPersistOrder: [Element] {
        sorted {
            guard let leftValue = $0.persistOrderId,
                  let rightValue = $1.persistOrderId else { return false }
            
            return leftValue < rightValue
        }
    }
}
