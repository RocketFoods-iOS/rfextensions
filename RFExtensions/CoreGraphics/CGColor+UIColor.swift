//
//  CGColor+UIColor.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 5/14/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension CGColor {
    open var uiColor: UIColor {
        UIColor(cgColor: self)
    }
}
