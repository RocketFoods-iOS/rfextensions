//
//  Success.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 7/10/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import SwiftMessages
import UIKit

public extension SwiftMessages {
    class var bottomSuccessConfig: Config {
        var config = SwiftMessages.defaultConfig
        
        config.preferredStatusBarStyle = .lightContent
        config.presentationContext = .automatic //.window(windowLevel: UIWindow.Level.statusBar)
        config.presentationStyle = .bottom
        
        return config
    }
    
    class var topSuccessConfig: Config {
        var config = SwiftMessages.defaultConfig
        
        config.preferredStatusBarStyle = .lightContent
        config.presentationContext = .automatic //.window(windowLevel: UIWindow.Level.statusBar)
        config.presentationStyle = .top
        
        return config
    }
    
    class func showBottomSuccess(_ content: Content, isHideOnTapEnabled: Bool = true,
                              isAutoHide: Bool = true, buttonTapHandler: ((_ button: UIButton?) -> Void)? = nil) {
        var config = SwiftMessages.bottomSuccessConfig
        
        if !isAutoHide {
            config.duration = .forever
        }
        
        show(config: config) {
            let warning = MessageView.viewFromNib(layout: .cardView)
            
            warning.configureTheme(backgroundColor: .mainLightGreen, foregroundColor: .white)
            warning.configureDropShadow()
            warning.configureContent(content)
            
            warning.buttonTapHandler = { _ in
                hide(id: warning.id)
                
                buttonTapHandler?(warning.button)
            }
            
            if isHideOnTapEnabled {
                warning.tapHandler = { _ in
                    hide(id: warning.id)
                }
            }
            
            return warning
        }
    }
    
    class func showTopSuccess(_ content: Content, isHideOnTapEnabled: Bool = true,
                                 isAutoHide: Bool = true, buttonTapHandler: ((_ button: UIButton?) -> Void)? = nil) {
        var config = SwiftMessages.topSuccessConfig
        
        if !isAutoHide {
            config.duration = .forever
        }
        
        show(config: config) {
            let warning = MessageView.viewFromNib(layout: .cardView)
            
            warning.configureTheme(backgroundColor: .mainLightGreen, foregroundColor: .white)
            warning.configureDropShadow()
            warning.configureContent(content)
            
            warning.buttonTapHandler = { _ in
                hide(id: warning.id)
                
                buttonTapHandler?(warning.button)
            }
            
            if isHideOnTapEnabled {
                warning.tapHandler = { _ in
                    hide(id: warning.id)
                }
            }
            
            return warning
        }
    }
}
