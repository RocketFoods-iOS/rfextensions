//
//  Date.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 31.08.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension Date {
    var dateText: String {
        let dateFormatter = DateFormatter()

        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd.MM.yyyy"

        return dateFormatter.string(from: self)
    }

    var timeText: String {
        let dateFormatter = DateFormatter()

        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "HH:mm"

        return dateFormatter.string(from: self)
    }

    var dateTimeText: String {
        let dateFormatter = DateFormatter()

        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm"

        return dateFormatter.string(from: self)
    }
    
    var timeGMTText: String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "HH:mm O"
        
        return dateFormatter.string(from: self)
    }
    
    var dateTimeGMTText: String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd.MM.yyyy HH:mm O"
        
        return dateFormatter.string(from: self)
    }
    
    var shortDateText: String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "dd.MM"
        
        return dateFormatter.string(from: self)
    }
    
    var dayMonthText: String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.locale = Locale(identifier: "ru_RU")
        dateFormatter.dateFormat = "d MMMM"
        
        return dateFormatter.string(from: self)
    }
}
