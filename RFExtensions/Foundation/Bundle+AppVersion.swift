//
//  Bundle+AppVersion.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 5/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension Bundle {
    class var appVersion: String {
        main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.0"
    }
}
