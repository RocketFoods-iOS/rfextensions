//
//  Double.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/8/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension Double {
    func remainder(precision: Int, _ rule: FloatingPointRoundingRule = .toNearestOrAwayFromZero) -> Int {
        Int((truncatingRemainder(dividingBy: 1) * pow(10, Double(precision))).rounded(rule))
    }
    
    func isEqual(_ to: Double, precision: Int) -> Bool {
        Int(self) == Int(to) && remainder(precision: precision) == to.remainder(precision: precision)
    }
}
