//
//  String+EmailValidation.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 4/10/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension String {
    var isValidEmail: Bool {
        let emailRegEx = "[A-Za-zА-Яа-я0-9._%+-]+@[A-Za-zА-Яа-я0-9.-]+\\.[A-Za-zА-Яа-я]{2,64}"

        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        
        return emailPred.evaluate(with: self)
    }
}
