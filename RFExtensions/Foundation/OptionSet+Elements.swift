//
//  OptionSet+Elements.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 25.10.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension OptionSet where RawValue: FixedWidthInteger {
    
    var elements: AnySequence<Self> {
        var remainingBits = rawValue
        var bitMask: RawValue = 1
        return AnySequence {
            return AnyIterator {
                while remainingBits != 0 {
                    defer { bitMask = bitMask &* 2 }
                    if remainingBits & bitMask != 0 {
                        remainingBits = remainingBits & ~bitMask
                        return Self(rawValue: bitMask)
                    }
                }
                return nil
            }
        }
    }
}
