//
//  Decimal.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/29/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension Decimal {
    var doubleValue: Double {
        Double(description) ?? 0
    }
    
    var intValue: Int {
        Int(doubleValue)
    }
    
    func description(with precision: Int, _ rule: FloatingPointRoundingRule? = nil) -> String {
        let doubleValue: Double = {
            if let rule = rule {
                return self.doubleValue.rounded(rule)
            } else {
                return self.doubleValue
            }
        }()
        
        return String(format: "%.\(precision)f", doubleValue)
    }
}
