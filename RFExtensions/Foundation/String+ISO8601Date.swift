//
//  String+ISO8601Date.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 5/30/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension String {
    var iso8601Date: Date? {
        let dateFormatter = ISO8601DateFormatter()

        dateFormatter.formatOptions = [
            .withInternetDateTime,
            .withTimeZone,
            .withFractionalSeconds,
            .withDashSeparatorInDate,
            .withColonSeparatorInTime,
            .withColonSeparatorInTimeZone
        ]

        return dateFormatter.date(from: self)
    }
}
