//
//  String+RubleSign.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 2/11/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension String {
    static var rubleSign: String { "₽" }
}
