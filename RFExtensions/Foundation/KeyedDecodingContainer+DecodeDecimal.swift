//
//  KeyedDecodingContainer.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/30/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension KeyedDecodingContainer {
    func decode(_ type: Decimal.Type, forKey key: KeyedDecodingContainer<K>.Key) throws -> Decimal {
        let doubleValue = try decode(Double.self, forKey: key)

        let stringValue = String(format: "%.2f", doubleValue)
        
        guard let decimalValue = Decimal(string: stringValue) else {
            return Decimal(doubleValue)
        }
        
        return decimalValue
    }

    func decodeIfPresent(_ type: Decimal.Type, forKey key: Self.Key) throws -> Decimal? {
        guard let doubleValue = try decodeIfPresent(Double.self, forKey: key) else {
            return nil
        }

        let stringValue = String(format: "%.2f", doubleValue)
        
        return Decimal(string: stringValue)
    }
}
