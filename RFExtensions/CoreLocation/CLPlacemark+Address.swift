//
//  CLPlacemark+Address.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 2/8/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import CoreLocation

extension CLPlacemark {
    public var address: String {
        var address = ""
        var hasCity = false
        var hasStreet = false
        
        if let city = locality {
            hasCity = true
            
            address += "\(city)"
        }
        
        if let street = thoroughfare {
            hasStreet = true
            
            address += "\(hasCity ? ", " : "")\(street)"
        }
        
        if let house = subThoroughfare {
            address += "\(hasStreet || hasCity ? ", " : "")\(house)"
        }
        
        return address
    }
}
