//
//  Result+IndexPaths.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 5/26/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import ListDiff

public extension List.Result {
    func deletes(for section: Int) -> [IndexPath] {
        deletes.map { IndexPath(item: $0, section: section) }
    }
    
    func inserts(for section: Int) -> [IndexPath] {
        inserts.map { IndexPath(item: $0, section: section) }
    }
    
    func moves(for section: Int) -> [(IndexPath, IndexPath)] {
        moves.map {
            (IndexPath(item: $0.from, section: section), IndexPath(item: $0.to, section: section))
        }
    }
}
