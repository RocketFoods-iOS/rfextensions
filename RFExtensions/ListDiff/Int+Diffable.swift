//
//  Int+Diffable.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 5/6/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import ListDiff

extension Int: Diffable {
    public var diffIdentifier: AnyHashable {
        return self
    }
}
