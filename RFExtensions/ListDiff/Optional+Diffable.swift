//
//  Optional+Diffable.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 10/9/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation
import ListDiff

extension Optional: Diffable where Wrapped: Diffable {
    public var diffIdentifier: AnyHashable {
        return self?.diffIdentifier
    }
}
