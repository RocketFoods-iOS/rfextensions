//
//  RevealOrderDelegate.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/3/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol RevealOrderDelegate: class {
    func revealOrder(_ orderId: Int)
    
    func showCheckoutSuccess(_ orderId: Int)
}
