//
//  CartItemCounterDelegate.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/10/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol CartItemCounterDelegate: class {
    func setCartCounter(_ value: Int)
}
