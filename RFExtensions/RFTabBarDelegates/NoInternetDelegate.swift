//
//  NoInternetDelegate.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 4/16/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol NoInternetDelegate: AnyObject {
    func presentNoInternet()
}
