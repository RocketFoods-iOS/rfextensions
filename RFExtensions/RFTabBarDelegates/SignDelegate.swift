//
//  SignDelegate.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 2/2/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol SignDelegate: Injectable {
    func signChoose()
    
    func signIn(completion: ((Bool) -> Void)?)
    
    func signOut()
}
