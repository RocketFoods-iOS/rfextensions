//
//  UIEdgeInsets.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 1/12/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

public extension UIEdgeInsets {
    var inverse: UIEdgeInsets {
        return UIEdgeInsets(top: -top, left: -left, bottom: -bottom, right: -right)
    }
}
