//
//  UITextView.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 23/02/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit

public extension UITextView {
    var numberOfCurrentlyDisplayedLines: Int {
        let size = systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)

        return Int((size.height - layoutMargins.top - layoutMargins.bottom) / (font?.lineHeight ?? 0))
    }

    func removeTextUntilSatisfying(maxNumberOfLines: Int) {
        while numberOfCurrentlyDisplayedLines > maxNumberOfLines {
            text = String(text.dropLast())
            layoutIfNeeded()
        }
    }
}
