//
//  UIColor+Disabled.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 2/23/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension UIColor {
    open var disabled: UIColor {
//        guard let brightness = brightness,
//            let alpha = alpha else { return self }
//
//        return UIColor(hue: 0, saturation: 0, brightness: brightness, alpha: alpha)
        
        return .mainLightGray
    }
    
    open var textDisabled: UIColor {
        return .mainGray
    }
}
