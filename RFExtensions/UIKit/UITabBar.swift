//
//  UITabBar.swift
//  RFExtensions
//
//  Created by Nikita Arutyunov on 3/29/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

public extension UITabBar {
    func removeItemsText() {
       if let items = items {
          for item in items {
             item.title = ""
          }
       }
    }
}
