.PHONY: help
help:
	@echo 'To update Carthage dependencies or build Carthage with current target, run the following commands:'
	@echo '  $$ make carthage-update'
	@echo '  $$ make carthage-build'

carthage-update:
	carthage update --platform iOS --use-xcframeworks --cache-builds --new-resolver

carthage-build:
	carthage build --platform iOS --use-xcframeworks --cache-builds --no-skip-current



clear-cached-build:
	rm -rf ${HOME}/Library/Developer/Xcode/DerivedData/*

clear-carthage-cache:
	rm -rf ${HOME}/Library/Caches/carthage
	rm -rf ${HOME}/Library/Caches/org.carthage.CarthageKit

clear-carthage:
	rm -rf Cartfile.resolved Carthage

clean: clear-cached-build clear-carthage-cache clear-carthage

all: carthage-update
